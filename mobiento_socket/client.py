import socket
import struct
import threading
from threading import Thread, Event
from queue import Queue
from typing import Callable
import time

from mobiento_socket import mobiento_message

STATE_CONNECT = "STATE_CONNECT"
STATE_LISTEN = "STATE_LISTEN"

LISTENER_WAITING = "LISTENER_WAITING"
LISTENER_RECEIVING = "LISTENER_RECEIVING"
MESSAGE_READY = "MESSAGE_READY"

MAX_BUFFER_SIZE = 4096

DEFAULT_MAX_RETRIES = 10
DEFAULT_TIMEOUT = 3
DEAFULT_HEARTBEAT_INTERVAL = 10

class ConnectionError(Exception):
    pass


class SocketError(Exception):
    pass


class Client:
    def __init__(
        self,
        host: str,
        port: int,
        fallback_handler: Callable,
        callback_handlers: dict = {},
        max_retries=DEFAULT_MAX_RETRIES,
        connection_timeout=DEFAULT_TIMEOUT,
        heartbeat_interval=DEAFULT_HEARTBEAT_INTERVAL
    ) -> None:
        self.host = host
        self.port = port
        self.fallback_handler = fallback_handler
        self.callback_handlers = callback_handlers
        self.max_retries = max_retries
        self.connection_timeout = connection_timeout
        self.heartbeat_interval = heartbeat_interval

        self.write_buffer = Queue()
        self.running = Event()
        self.connected = Event()
        self.last_heartbeat = None

    def start(self) -> None:
        self.socket_loop = Thread(target=self._loop)
        self.running.set()
        self.socket_loop.start()

        self.connected.wait()

    def send(self, message: dict) -> None:
        self.connected.wait()
        mobiento_message.validate(message)
        self.write_buffer.put(message)

    def close(self) -> None:
        self.running.clear()
        self.connection.shutdown(socket.SHUT_RDWR)

    def disconnect(self) -> None:
        print('disconnected')
        self.connected.clear()

    def _connect(self) -> None:
        retries = 0
        while not self.connected.is_set():
            if self.max_retries is not None and retries >= self.max_retries:
                raise ConnectionError(
                    "Could not connect during {0} retries.".format(self.max_retries)
                )
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.connect((self.host, self.port))
                sock.setblocking(False)
                self.connection = sock
                self.connected.set()
            except socket.gaierror:
                print("No interface available, retrying..")
                time.sleep(self.connection_timeout)
                retries += 1
            except ConnectionRefusedError:
                print(
                    "Connection refused, retrying.. {0}".format(
                        retries
                    )
                )
                time.sleep(self.connection_timeout)
                retries += 1
            time.sleep(0.05)

    def _loop(self):
        self.last_heartbeat = time.time()
        while self.running.is_set():
            if not self.connected.is_set():
                self._connect()
            if not self.write_buffer.empty():
                message = self.write_buffer.get(False)
                if message:
                    data = mobiento_message.encode(message)
                    self.connection.sendall(data)
            now = time.time()
            td = now - self.last_heartbeat
            if td >= self.heartbeat_interval:
                self.last_heartbeat = now
                self.send({"type": "HB", "payload": ""})
            try:
                data = self._recv_message(self.connection)
                if not data:
                    raise SocketError('Socket returned empty.')
                message = mobiento_message.decode(data)
                if message:
                    callback = self.callback_handlers.get(message["type"], None)
                    if callback:
                        callback(message["payload"])
                    else:
                        self.fallback_handler(message)
            except ValueError as e:
                print(e)
            except SocketError as e:
                print(e)
                self.disconnect()
            except socket.timeout as e:
                print(e)
                self.disconnect()
            except OSError as e:
                pass
            except AttributeError as e:
                print(e)

            time.sleep(0.01)

    def _recv_message(self, read_buffer):
        raw_msglen = self._recv_all(read_buffer, 4)
        if not raw_msglen:
            return None
        msg_len = struct.unpack(">i", raw_msglen)[0]
        return self._recv_all(read_buffer, msg_len)

    def _recv_all(self, read_buffer, size):
        data = b""
        while len(data) < size:
            packet = read_buffer.recv(size - len(data))
            if not packet:
                return None
            data += packet
        return data
