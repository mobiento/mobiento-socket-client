import json
import struct
from uuid import uuid4


def has_payload(data):
    if "payload" in data:
        return True
    return False


def has_type(data):
    if "type" in data:
        return True
    return False


def validate(message):
    if not has_payload(message):
        raise ValueError("Message is missing field: payload")
    if not has_type(message):
        raise ValueError("Message is missing field: type")


def decode(data):
    message = json.loads(data.decode())
    try:
        validate(message)
        return message
    except ValueError as e:
        return None


def generate_message_id():
    return uuid4()


def encode(message):
    data = json.dumps(message).encode()
    size = struct.pack(
        ">i", len(data)
    )  # produces a 32 bit integer in the format of a bytes array of length 4
    payload = size + data
    return payload


def check_valid_data_struct(data):
    return has_payload(data) and has_type(data)


def build(message_type, messasge_data, message_id=None):
    message = {"type": message_type, "payload": messasge_data}
    if message_id:
        message["id"] = message_id
    return message
