import socket
import time
from threading import Thread

import pytest

from mobiento_socket import client


class FakeEchoServer(Thread):
    def __init__(self,):
        Thread.__init__(self)

    def run(self):
        self.server_socket = socket.socket()
        self.server_socket.bind(("127.0.0.1", 8888))
        self.server_socket.listen(0)
        print("MOCK Listening")
        conn, addr = self.server_socket.accept()
        while True:
            data = conn.recv(1024)
            print("MOCK Recveived: {0}".format(data))
            if not data:
                break
            conn.sendall(data)
        self.server_socket.close()

    def stop(self):
        print("MOCK Stopped")
        self.server_socket.close()
        #  self.server_socket.shutdown(socket.SHUT_RDWR)


@pytest.fixture(autouse=False)
def server():
    server_thread = FakeEchoServer()
    server_thread.start()
    yield
    server_thread.stop()


def test_client_start(server, mocker):
    fallback_stub = mocker.stub(name="on_message_stub")
    c = client.Client("127.0.0.1", 8888, fallback_stub)
    c.start()
    c.close()


def test_client_fallback_handler(server, mocker):
    fallback_stub = mocker.stub(name="on_message_stub")
    c = client.Client("127.0.0.1", 8888, fallback_stub)
    c.start()
    message = {"type": "test", "payload": ""}
    c.send(message)
    time.sleep(1)
    c.close()
    fallback_stub.assert_called_once_with(message)


def test_client_raises_exception(server, mocker):
    fallback_stub = mocker.stub(name="on_message_stub")
    with pytest.raises(ValueError):
        c = client.Client("127.0.0.1", 8888, fallback_stub)
        c.start()
        message = {"nottype": "test", "payload": ""}
        c.send(message)
    c.close()


def test_client_multiple_messages(server, mocker):
    fallback_stub = mocker.stub(name="on_message_stub")
    c = client.Client("127.0.0.1", 8888, fallback_stub)
    c.start()
    for x in range(10):
        message = {"type": "test", "payload": ""}
        c.send(message)
    time.sleep(2)
    c.close()
    assert fallback_stub.call_count == 10


def test_client_callback_handler(server, mocker):
    fallback_stub = mocker.stub(name="fallback_stub")
    callback_stub = mocker.stub(name="callback_stub")
    callback_handlers = {"call": callback_stub}
    c = client.Client("127.0.0.1", 8888, fallback_stub, callback_handlers)
    c.start()
    message = {"type": "call", "payload": "hej"}
    c.send(message)
    time.sleep(0.5)
    c.close()
    callback_stub.assert_called_once_with("hej")
    assert not fallback_stub.called


def test_client_heartbeat(server, mocker):
    fallback_stub = mocker.stub(name="on_message_stub")
    c = client.Client("127.0.0.1", 8888, fallback_stub, heartbeat_interval=1)
    c.start()
    message = {"type": "HB", "payload": ""}
    time.sleep(1.5)
    c.close()
    fallback_stub.assert_called_once_with(message)


def test_client_persistent(mocker):
    fallback_stub = mocker.stub(name="fallback_stub")
    callback_stub = mocker.stub(name="callback_stub")
    callback_handlers = {"call": callback_stub}

    def delayed_server():
        time.sleep(3)
        server_thread = FakeEchoServer()
        server_thread.start()
        time.sleep(3)
        server_thread.stop()

    delayed_thread = Thread(target=delayed_server)
    delayed_thread.start()
    c = client.Client("127.0.0.1", 8888, fallback_stub, callback_handlers)
    c.start()
    message = {"type": "call", "payload": "hej"}
    print("now sending")
    c.send(message)
    time.sleep(0.5)
    c.close()
    time.sleep(7)
    callback_stub.assert_called_once_with("hej")
    assert not fallback_stub.called
