# Mobiento Socket Client
`This library is still a work in progress. Don't use for anything other than development, or inspiration.`
A library for a persisted raw TCP socket layer.
With some convenient callback syntax to make life easier. Really a poorly done RPC protocol.. Just use gRPC or zerorpc instead.

## Installation
`pip install -r requirements.txt`

## Test
`python -m pytest`

## Usage
Simply create a instance of `Client` with appropriate callback handlers that can take care of messages. You need to have a fallback handler defined that catches the rest.


## Goals / TODOs
* Separate `Client` into a strictly persisted socket, with a wrapper around it that applies application logic such as the callbacks.
* Use messagepack instead of json.
* Have heartbeat built in to persisted socket to detect disconnects earlier.
