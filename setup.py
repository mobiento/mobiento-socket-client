from setuptools import setup

VERSION = "0.0.11"


setup(
    name="Mobiento Socket",
    version=VERSION,
    packages=["mobiento_socket"],
    package_dir={"mobiento_socket": "mobiento_socket"},
    include_package_data=True,
    url="",
    license="",
    author="Oskar Lundh",
    author_email="oskar.lundh@mobiento.com",
    description="",
    install_requires=[],
    zip_safe=False,
    platforms="any",
)
